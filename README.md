# json_reader_setkin

This is a Scala Spark 'hello world' app, that takes a wine reviews dataset file as an argument
(https://storage.googleapis.com/otus_sample_data/winemag-data.json.tgz), loads it into RDD and then parses it's rows 
using json4s library, copies each parsed row into case class and finally prints it into stdout.

P.S. It is assumed inside the app that Spark is run locally and located at /usr/local/spark. 

## Accessing the library

*How to access the code*

## Documentation

*A link to the documentation*

## How to contribute

*How others can contribute to the project*