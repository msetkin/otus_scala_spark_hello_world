package com.github.mrpowers.my.cool.project
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark._
import org.apache.spark.SparkContext
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.json4s.Formats
import org.json4s.jackson.Serialization
import org.json4s.DefaultFormats._


object JsonReader {

  case class wineReview(id: Option[Int], country: Option[String], points: Option[Int], price:Option[Float], title:Option[String], variety:Option[String], winery:Option[String])

  def main(args: Array[String]) {
    if (args.length == 0) {
      println("dude, i need at least one parameter")
    }
    val filename = args(0)
    val sc = new SparkContext( "local", "spark session", "/usr/local/spark", Nil, Map())
    val rdd = sc.textFile(filename)

    def getValue(x: String): (wineReview) = {
      implicit val formats = DefaultFormats
      val obj = parse(x).extract[wineReview]
      (obj)
    }

    val mapFile = rdd.map(row => getValue(row))
    mapFile.foreach(println)
    }
}
